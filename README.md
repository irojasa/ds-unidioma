# ds-unidioma

DS Project for 2021-2 semester -  Universidad Nacional de Colombia

## Name
UN Idioma v1.0.0.

## Description
App focused in phonetic analysis, and language learning for everyone who wants to learn in a different way.
Also in this app, you can control your progress using notes for new words, goals and everything you imagine.

## Badges
![license](https://img.shields.io/badge/license-GPL-blue "gpl")  ![dependencies](https://img.shields.io/badge/javafx-v13-green "javafx")  ![version](https://img.shields.io/badge/version-1.0.0-bluen "version")

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
To use this software, you have to install JRE 11+ and execute the .jar file with the Java platform, or if you want to modify the source code, clone or fork the code, then we recommend use eclipse and add the following configuration for Maven:

```
cd existing_folder
git clone --mirror https://gitlab.com/irojasa/ds-unidioma.git
In eclipse, add the config:
goals: clean javafx:run
```
## Support
Contact us in this project, using pull requests.

## Authors and acknowledgment
Authors:
- Juan Alvarado
- Ivan Rojas

## License
GNU GPLv3

## Project status
Development

