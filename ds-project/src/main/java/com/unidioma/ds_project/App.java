package com.unidioma.ds_project;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;

import com.unidioma.ds_project.models.LinkedListGeneral;
import com.unidioma.ds_project.models.ListBasedArray;
import com.unidioma.ds_project.models.NodeGeneral;
import com.unidioma.ds_project.models.Note;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("principalscreen"), 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("./views/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
    	Instant start = Instant.now();
    	/*Note head = new Note("dsds", "dfd", "dsds", LocalDate.now());
        LinkedListGeneral<Note> test = new LinkedListGeneral<>();
        for (int i = 0; i < 50000; i++) {
        	if(i == 0 || i == 1000) {
        		System.out.println("hola");
        	}
			Note temp = new Note("dsds", "dfd", "dsds", LocalDate.now());
			test.add(i, temp);
		}*/
    	ListBasedArray<Note> test = new ListBasedArray<>(100000000);
    	for (int i = 0; i < 100000000; i++) {
    		int ix = (int) (int)(Math.random()*test.);
        	if(i == 0 || i == 1000) {
        		System.out.println("hola");
        	}
			Note temp = new Note("dsds", "dfd", "dsds", LocalDate.now());
			test.add(i, temp);
		}
        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        System.out.println(timeElapsed);
  
        launch();
        
    }

}