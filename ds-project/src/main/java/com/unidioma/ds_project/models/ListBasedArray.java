package com.unidioma.ds_project.models;

public class ListBasedArray<T> implements IList<T> {
	
	protected T[] items;
	protected int size;
	
	@SuppressWarnings("unchecked")
	public ListBasedArray(int capacity) {
		if(capacity < 1) {
			throw new IllegalArgumentException();
		}
		items = (T[]) new Object[capacity];
		size = 0;
	}
	
	public boolean isEmpty() {
		return this.size == 0;
	}
	
	public int size() {
		return this.size;
	}
	
	public void checkIndex(int i) {
		if(i < 0 || i >= size) {
			throw new IndexOutOfBoundsException();
		}
	}

	@Override
	public T get(int index) {
		// TODO Auto-generated method stub
		checkIndex(index);
		return items[index];
	}

	@Override
	public int indexOf(T searchedValue) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public T remove(int index) {
		// TODO Auto-generated method stub
		checkIndex(index);
		
		T deleted = items[index];
		for (int i = index+1; i < size; i++) {
			items[i-1] = items[i];
			
		}
		items[--size] = null;
		return deleted;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void add(int index, T theElement) {
		// TODO Auto-generated method stub
		if(index < 0 || index > size) {
			throw new IndexOutOfBoundsException();
		}
		
		if(size == items.length)
		{
			T[] old = items;
			items = (T[]) new Object[2*size];
			System.arraycopy(old, 0, items, 0, size);
		}
		for (int i = size - 1; i >= index; i--) {
			items[i+1] = items[i];
		}
		items[index] = theElement;
		size++;
	}

}
