package com.unidioma.ds_project.models;

public class NodeGeneral<T> {

	private T data;
	private NodeGeneral<T> next;

	public NodeGeneral() {
		this(null, null);
	}

	public NodeGeneral(T data) {

		this(data, null);

	}

	public NodeGeneral(T data, NodeGeneral<T> next)

	{
		this.setData(data);
		this.setNext(next);

	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public NodeGeneral<T> getNext() {
		return next;
	}

	public void setNext(NodeGeneral<T> next) {
		this.next = next;
	}
}
