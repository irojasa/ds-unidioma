package com.unidioma.ds_project.models;

public class LinkedListGeneral<T> implements IList<T> {
	
	protected NodeGeneral<T> head;
	protected int size;
	
	public LinkedListGeneral() {
		head = null;
		size = 0;
	}
	
	public boolean isEmpty() {
		return this.size == 0;
	}
	
	public int size() {
		return this.size;
	}
	
	public void checkIndex(int i) {
		if(i < 0 || i >= size) {
			throw new IndexOutOfBoundsException();
		}
	}
	@Override
	public T get(int index) {
		// TODO Auto-generated method stub
		checkIndex(index);
		
		NodeGeneral<T> actualNode = head;
		for (int i = 0; i < index; i++) {
			actualNode = actualNode.getNext();
		}
		return actualNode.getData();
	}

	@Override
	public int indexOf(T searchedValue) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public T remove(int index) {
		// TODO Auto-generated method stub
		checkIndex(index);
		T deleted;
		if(index == 0)
		{
			deleted = head.getData();
			head = head.getNext();
		}
		else {
			NodeGeneral<T> predecessor = head;
			
			for (int i = 0; i < index - 1; i++) {
				predecessor = predecessor.getNext();
			}
			deleted = predecessor.getNext().getData();
			predecessor.setNext(predecessor.getNext().getNext());
		}
		size--;
		return deleted;
	}

	@Override
	public void add(int index, T theElement) {
		// TODO Auto-generated method stub
		if(index < 0 || index > size) {
			throw new IndexOutOfBoundsException();
		}
		
		if(index == 0)
		{
			head = new NodeGeneral<T>(theElement, head);
		}
		else {
			NodeGeneral<T> predecessor = head;
			
			for (int i = 0; i < index - 1; i++) {
				predecessor = predecessor.getNext();
			}
			predecessor.setNext(new NodeGeneral<T>(theElement, predecessor.getNext()));
		}
		size++;
	}
}
