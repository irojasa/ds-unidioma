package com.unidioma.ds_project.models;

import java.time.LocalDate;

public class Note {
	private String type;
	private String title;
	private String description;
	private LocalDate dateIn;
	public Note(String type, String title, String description, LocalDate dateIn) {
		super();
		this.type = type;
		this.title = title;
		this.description = description;
		this.dateIn = dateIn;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LocalDate getDateIn() {
		return dateIn;
	}
	public void setDateIn(LocalDate dateIn) {
		this.dateIn = dateIn;
	}
	
	
}
