package com.unidioma.ds_project.models;

public class Node {
	
	String data;
	Node next;
	Node(String d) {
        data = d;
        next = null;
    }
	
	   public static  Node insert(Node head,String data) {
	        if (head==null) {
	            Node n0=new Node(data);
	            head=n0;
	            
	        }
	        else{
	            Node comienzo=head;
	            while(comienzo.next!=null){
	                comienzo=comienzo.next;
	                
	            }
	            comienzo.next=new Node(data);
	            
	        }
	        return head;
	    }
	   
		public static void printListaEnlazada(Node head) {
	        Node start = head;
	        while(start != null) {
	            System.out.print(start.data + " ");
	            start = start.next;
	        }
	    }
		
		public static Node insertarFrase(Node head,String frase) {
			
			String palabra="";
			for (int n = 0; n <frase.length (); n++) {
				char c = frase.charAt (n);
				if(c==' ') {
					head= Node.insert(head, palabra);

					palabra="";
					
				}
				else {
					palabra=palabra+String.valueOf(c);
					
				}
				
			}
			head=Node.insert(head, palabra);
			return head;
		}
		
		public static int size(Node head) {
			int contador=0;
			while(head!=null) {
				contador+=1;
				head=head.next;
			}
			return contador;
			
		}

}
