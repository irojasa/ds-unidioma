package com.unidioma.ds_project.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;



public class metodos {
	Connection con;

public static String [][] ipa= {{"AA","ɑ"},{"AA0","ɑ"},{"AA1","ɑ"},{"AA2","ɑ"},{"AE","æ"},{"AE0","æ"},{"AE1","æ"},{"AE2","æ"},{"AH","ʌ"},{"AH0","ʌ"},{"AH1","ʌ"},{"AH2","ʌ"},{"AO0","ɔ"},{"AO1","ɔ"},{"AO2","ɔ"},{"AO","ɔ"},
			{"AW","aʊ"},{"AW0","aʊ"},{"AW1","aʊ"},{"AW2","aʊ"},{"AX","ə"},{"AXR","er"},{"AY","aɪ"},{"AY0","aɪ"},{"AY1","aɪ"},{"AY2","aɪ"},{"B","b"},{"CH","tʃ"},{"D","d"},{"DH","ð"},{"DX","ɾ"},{"EH","ɛ"},{"EH0","ɛ"},{"EH1","ɛ"},
			{"EH2","ɛ"},{"EL","l̩"},{"EM","m̩"},{"EN","n̩"},{"ER","ɝ"},{"ER0","ɝ"},{"ER1","əː"},{"ER2","ɝ"},{"EY","eɪ"},{"EY0","eɪ"},{"EY1","eɪ"},{"EY2","eɪ"},{"F","f"},{"G","ɡ"},{"HH","h"},{"IH","ɪ"},{"IH0","ɪ"},{"IH1","ɪ"},{"IH2","ɪ"},
			{"IX","ɨ"},{"IY","i"},{"IY0","i"},{"IY1","i"},{"IY2","i"},{"JH","dʒ"},{"K","k"},{"L","l"},{"M","m"},{"N","n"},{"NG","ŋ"},{"OW","oʊ"},{"OW0","oʊ"},{"OW1","oʊ"},{"OW2","oʊ"},{"OY","ɔɪ"},{"OY0","ɔɪ"},{"OY1","ɔɪ"},{"OY2","ɔɪ"},
			{"P","p"},{"Q","ʔ"},{"R","r"},{"S","s"},{"SH","ʃ"},{"T","t"},{"TH","θ"},{"UH","ʊ"},{"UH0","ʊ"},{"UH1","ʊ"},{"UH2","ʊ"},{"UW","u"},
			{"UW0","u"},{"UW1","u"},{"UW2","u"},{"UX","ʉ"},{"V","v"},{"W","w"},{"WH","ʍ"},{"Y","j"},{"Z","z"},{"ZH","ʒ"}};

public metodos() {
	try {
		Class.forName("com.mysql.jdbc.Driver");
    	this.con=DriverManager.getConnection("jdbc:mysql://localhost:3306/ingles", "root", "contrasena");
    	
		
	}
	catch(Exception e){
		System.err.print("error "+e);

		
		
	}
	
	
}

public static String transcribirPalabra(metodos cn, String frase) {

	String transcripcion="";



	Statement st;
	ResultSet rs;
	try {

		st=cn.con.createStatement();
		
		ListaArray lista = new ListaArray(frase);
		
		int len=lista.size();
		
		for(int x=0;x<len;x++) {
			try {

				rs=st.executeQuery("select * from palabras where palabra='"+lista.getElemento(x)+"'");
    			rs.next();
    			String pronunciacion=rs.getString(3);
    			ArrayList<String> listaPron = new ArrayList<String>(Arrays.asList(pronunciacion.split(" ")));

    			int len2=listaPron.size();

                if (x==len-1) {
                	transcripcion=transcripcion+(metodos.Arreglo(len2, listaPron));
                	
                }
    			else{
    				transcripcion=transcripcion+(metodos.Arreglo(len2, listaPron)+"  -  ");
    			}
    			
				
			}
			catch(Exception e) {
				transcripcion=transcripcion+"  -  ";
				
			}
			
		}

		
	}
	catch(Exception e){
		transcripcion="error";
		e.printStackTrace();
		
		
	}
	return transcripcion;

	
}

	
	public static String pila(int tamanoLista, ArrayList<String> listaPronunciacionPalabra) {
	    Stack pila=new Stack();
	    String transcripcion="";

		

		for (int x=tamanoLista-1;x>-1;x--) {
			pila.push(listaPronunciacionPalabra.get(x));	
			
		}


		
		while(pila.empty()==false) {
			String subCadena=(String) pila.peek();


			for (int y=0;y<94;y++) {

				if(subCadena.equals(ipa[y][0])) {
					transcripcion=transcripcion+ipa[y][1];
					break;
					
				}			
			
			}
			pila.pop();

				
		}
		return transcripcion;
		
		
	}
	
	public static String Arreglo(int tamanoLista, ArrayList<String> listaPronunciacionPalabra) {
	    String[] Arreglo=new String[tamanoLista];
	    String transcripcion="";
		

		for (int x=0;x<tamanoLista;x++) {
			Arreglo[x]=	listaPronunciacionPalabra.get(x);
			
			
		}


		
		for(String subCadena:Arreglo) {
			for (int y=0;y<94;y++) {

				if(subCadena.equals(ipa[y][0])) {
					transcripcion=transcripcion+ipa[y][1];
					break;
					
				}			
			
			}
			
		}
		return transcripcion;
		
		
	}
	
	public static String Arreglo2(int tamanoLista, ArrayList<String> listaPronunciacionPalabra) {
	    String[] Arreglo=new String[tamanoLista];
	    String transcripcion="";
		

		for (int x=0;x<tamanoLista;x++) {
			Arreglo[x]=	listaPronunciacionPalabra.get(x);
			
			
		}


		
		for(String subCadena:Arreglo) {
			for (int y=0;y<94;y++) {

				if(subCadena.equals(ipa[y][0])) {
					transcripcion=transcripcion+ipa[y][1];
					break;
					
				}			
			
			}
			
		}
		return transcripcion;
		
		
	}

	public static String listaEnlazada(int tamanoLista, ArrayList<String> listaPronunciacionPalabra) {
		Node head=null;
	    String transcripcion="";

		for (int x=0;x<tamanoLista;x++) {
			head=head.insert(head,listaPronunciacionPalabra.get(x) );
			

			
			
		}




		
		for(int y=0;y<tamanoLista;y++) {
			String subCadena=head.data;

			for (int z=0;z<94;z++) {

				if(subCadena.equals(ipa[z][0])) {
					transcripcion=transcripcion+ipa[z][1];
					break;
					
				}			
			
			}
			head=head.next;
			
		}
		return transcripcion;
		
		
	}
}
