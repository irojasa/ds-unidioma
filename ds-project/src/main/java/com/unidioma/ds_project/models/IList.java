package com.unidioma.ds_project.models;

/*
 * ADT Specification
 */
public interface IList<T> {
	T get(int index);

	int indexOf(T searchedValue);

	T remove(int index);

	void add(int index, T theElement);

	String toString();

}
