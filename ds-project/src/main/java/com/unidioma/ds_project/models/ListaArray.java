package com.unidioma.ds_project.models;

public class ListaArray {
	
	private String[] listaPalabras=new String[1000000];
	private int ultimo=-1;
	
	public ListaArray(String frase) {
		String palabra="";
		for (int n = 0; n <frase.length (); n++) {
			char c = frase.charAt (n);
			if(c==' ') {
				ultimo++;
				listaPalabras[ultimo]=palabra;

				palabra="";
				
			}
			else {
				palabra=palabra+String.valueOf(c);
				
			}
			
		}
		ultimo++;
		listaPalabras[ultimo]=palabra;

		
	}
	
	public int size () {
		
		return ultimo+1;
	}
	
	public String getElemento(int indice) {
		return listaPalabras[indice];
		
		
	}
	public String printLista() {
		String lista="[";
		for(int x=0;x<ultimo+1;x++) {
			if (x==ultimo) {
				lista+=listaPalabras[x]+"]";
				
			}
			else {
				lista+=listaPalabras[x]+",";
			}
			
			
		}
		return lista;
		
	}

}
