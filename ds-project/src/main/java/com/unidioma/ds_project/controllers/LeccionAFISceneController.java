package com.unidioma.ds_project.controllers;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

public class LeccionAFISceneController {
	String[][] lista= {{"rose -->  [ rəʊ_ ]","z","s","rose.mp3"},
			{"password -->  [ pæ_wərd ]","s","z","password.mp3"},
			{"parrot -->  [ _ɛrət ]","p","b","parrot.mp3"},
			{"rabbit -->  [ ræ_ət ]","b","p","rabbit.mp3"}};
	int ver=0;
	double progreso=1.0/4.0;
	int correctas=0;
	Queue<Integer> cola;
	
	@FXML
	Label labelPalabra;
	@FXML
	Button boton1,boton2,botonEscuchar,botonCerrar;
	@FXML
	ProgressBar barra;
	
	
	private Stage stage;
	
	private Scene scene;
	private Parent root;
	
	public void setPagina(Queue<Integer> cola,int correctas) {
		this.cola=cola;
		this.correctas=correctas;
		labelPalabra.setText(lista[cola.peek()][0]);
		boton1.setText(lista[cola.peek()][1]);
		boton2.setText(lista[cola.peek()][2]);
		barra.setProgress(progreso*(correctas+1));
		
		
		
	}
	
	public void opcionCorrecta(ActionEvent e) throws IOException{
		Alert a = new Alert(Alert.AlertType.INFORMATION);
		a.setContentText("Respuesta correcta!!!");
		a.showAndWait();
		correctas++;

		
		this.cola.poll();
		
		if (cola.isEmpty()) {
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("AFIScene.fxml"));
		root=loader.load();

		
	    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
		stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
		Alert b = new Alert(Alert.AlertType.INFORMATION);
		b.setContentText("Lección completada!!!");
		b.showAndWait();
		}
		else {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("LeccionAFIScene.fxml"));
			root=loader.load();
			
			LeccionAFISceneController SceneController1=loader.getController();
			SceneController1.setPagina(this.cola,this.correctas);
			
		    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
			stage = (Stage)((Node)e.getSource()).getScene().getWindow();
			scene=new Scene(root);
			stage.setScene(scene);
			stage.show();
			
			
		}
		
	}
	
public void opcionIncorrecta(ActionEvent e) throws IOException{
	
	Alert a = new Alert(Alert.AlertType.INFORMATION);
	a.setContentText("Respuesta incorrecta!!!");
	a.showAndWait();
		this.cola.add(this.cola.peek());
		this.cola.poll();
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("LeccionAFIScene.fxml"));
		root=loader.load();
		
		LeccionAFISceneController SceneController1=loader.getController();
		SceneController1.setPagina(this.cola,this.correctas);
		
	    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
		stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
		
		
	}

public void escuchar(ActionEvent e) throws IOException{
	//String fileName = "city.mp3";
	//playSound(fileName);
	String s=lista[cola.peek()][3];

	AudioClip note = new AudioClip(this.getClass().getResource(s).toString());

	note.play();
}
public void cerrar(ActionEvent e) throws IOException{
	FXMLLoader loader = new FXMLLoader(getClass().getResource("AFIScene.fxml"));
	root=loader.load();

	
    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
	stage = (Stage)((Node)e.getSource()).getScene().getWindow();
	scene=new Scene(root);
	stage.setScene(scene);
	stage.show();
	
}

}

