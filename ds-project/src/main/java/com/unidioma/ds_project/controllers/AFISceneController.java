package com.unidioma.ds_project.controllers;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class AFISceneController {
	
	@FXML 
	
	private Button boton1 ;
	
	private Stage stage;
	
	private Scene scene;
	private Parent root;
	
	
	public void mostrarLeccion(ActionEvent e) throws IOException {
		//Queue<Integer> cola=new LinkedList<Integer>();
		//cola.add(0);
		//cola.add(1);
		//cola.add(2);
		//cola.add(3);
		int[] arreglo= {0,1,2,3};
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("TeoriaAFIScene.fxml"));
		root=loader.load();
		
		TeoriaAFISceneController SceneController2=loader.getController();
		SceneController2.setPagina(0, arreglo);
		
	    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
		stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
		
		

	}
	
	public void mostrarLeccionReglas(ActionEvent e) throws IOException {
		//Queue<Integer> cola=new LinkedList<Integer>();
		//cola.add(0);
		//cola.add(1);
		//cola.add(2);
		//cola.add(3);
		int[] arreglo= {0,1,2,3};
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("ReglasFoneticasScene.fxml"));
		root=loader.load();
		
		ReglasFoneticasSceneController SceneController2=loader.getController();
		SceneController2.setPagina(0, arreglo);
		
	    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
		stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
		
		

	}
	
	public void cerrar(ActionEvent e) throws IOException{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("FoneticaScene.fxml"));
		root=loader.load();

		
	    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
		stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
		
	}
	
	

}
