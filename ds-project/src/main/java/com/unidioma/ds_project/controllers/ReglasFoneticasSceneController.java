package com.unidioma.ds_project.controllers;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.media.AudioClip;
import javafx.stage.Stage;

public class ReglasFoneticasSceneController {
	
	String[][] lista= {{"La B es silenciosa si viene despu�s de la M y esta al final de la palabra","Thumb (pulgar)","thumb.mp3"},
			{"La G es muda cuando est� seguida de una N","Sign ( se�al)","sign.mp3"},
			{"La N es muda si viene despu�s de la M al final de una palabra","Column (columna)","column.mp3"},
			{"la W es muda al principio de una palabra si sigue una R","Write (escribir)","write.mp3"}};
	int ver=0;
	String audio;
	int[] arreglo=new int[4];
	double progreso=1.0/4.0;
	
	private Stage stage;
	
	private Scene scene;
	private Parent root;
	
	@FXML
	Label labelEnunciado,labelEjemplo;

	@FXML
	ProgressBar barra;
	
	public void setPagina(int n, int[] arreglo) {
		this.arreglo=arreglo;
		this.ver=n;
		barra.setProgress((n+1)*progreso);

		labelEnunciado.setText(lista[arreglo[n]][0]);
		labelEjemplo.setText(lista[arreglo[n]][1]);
		this.audio=lista[this.arreglo[this.ver]][2];


		
		
	}
	
	public void escuchar(ActionEvent e) throws IOException{
		//String fileName = "city.mp3";
		//playSound(fileName);
		String s=audio;

		AudioClip note = new AudioClip(this.getClass().getResource(s).toString());

		note.play();
	}
	
	public void pasarPagina(ActionEvent e) throws IOException{
		
		if (this.ver<3) {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("ReglasFoneticasScene.fxml"));
		root=loader.load();
		
		ReglasFoneticasSceneController SceneController1=loader.getController();
		SceneController1.setPagina(ver+1,this.arreglo);
		
	    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
		stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
		}
		else {
			Alert a = new Alert(Alert.AlertType.INFORMATION);
			a.setContentText("Leccion completada");
			a.showAndWait();
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("AFIScene.fxml"));
			root=loader.load();

			
		    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
			stage = (Stage)((Node)e.getSource()).getScene().getWindow();
			scene=new Scene(root);
			stage.setScene(scene);
			stage.show();
			
		}
			
		}
	public void cerrar(ActionEvent e) throws IOException{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("AFIScene.fxml"));
		root=loader.load();

		
	    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
		stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
		
	}


}

