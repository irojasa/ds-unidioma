package com.unidioma.ds_project.controllers;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FoneticaSceneController {
	
	private Stage stage;
	
	private Scene scene;
	private Parent root;
	
	public void AlfabetoFonetico(ActionEvent e) throws IOException{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("AFIScene.fxml"));
		root=loader.load();

		
	    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
		stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
		
	}

	public void TranscripcionFonetica(ActionEvent e) throws IOException{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("TranscripcionScene.fxml"));
		root=loader.load();

		
	    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
		stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
		
	}

}

