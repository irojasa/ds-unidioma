package com.unidioma.ds_project.controllers;

import java.io.IOException;

import com.unidioma.ds_project.App;
import com.unidioma.ds_project.models.metodos;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class TranscripcionSceneController {
	
	@FXML
	
	private TextArea texto1,texto2;
	metodos cn=new metodos();
	
	public void mostrarTranscripcion(ActionEvent e){
		String texto=metodos.transcribirPalabra(cn,texto1.getText());
        texto2.setText(texto);
		
		
	}
	
	public void cerrar(ActionEvent e) throws IOException{
		App.setRoot("principalscreen");
		
	}

}

