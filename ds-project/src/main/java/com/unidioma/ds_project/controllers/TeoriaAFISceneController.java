package com.unidioma.ds_project.controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.Stack;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

public class TeoriaAFISceneController  {
	String[][] lista= {{"Consonantes 1","/p/","Parecido al sonido de la “p” española.","help (hɛlp)","help.mp3"},
			{"Consonantes 1","/b/","Sonido de la “b” española.","ball (bɔl)","ball.mp3"},
	{"Consonantes 1","/s/","Parecido al sonido de la “s”española.","city (sɪdi)","city.mp3"},
	{"Consonantes 1","/z/","Sonido parecido a una “s” que vibra, como un zumbido.","reason (rizən)","reason.mp3"}};
	int ver=0;
	String audio;
	int[] arreglo=new int[4];
	double progreso=1.0/4.0;
	
	@FXML
	Label labelTitulo,labelSimbolo,labelCaracteristica,labelEjemplo;
	@FXML
	Button botonEscuchar,botonPasar;
	@FXML
	ProgressBar barra;
	
	
	private Stage stage;
	
	private Scene scene;
	private Parent root;
	
	

	
	//Media media=new Media(new File(path).toURI().toString());
	
	//MediaPlayer mediaPlayer = new MediaPlayer(media);
	
	public void setPagina(int n, int[] arreglo) {
		this.arreglo=arreglo;
		this.ver=n;
		barra.setProgress((n+1)*progreso);

		labelTitulo.setText(lista[arreglo[n]][0]);
		labelSimbolo.setText(lista[arreglo[n]][1]);
		labelCaracteristica.setText(lista[arreglo[n]][2]);
		labelEjemplo.setText(lista[arreglo[n]][3]);
		this.audio=lista[this.arreglo[this.ver]][4];


		
		
	}
	
	public void escuchar(ActionEvent e) throws IOException{
		//String fileName = "city.mp3";
		//playSound(fileName);
		String s=audio;

		AudioClip note = new AudioClip(this.getClass().getResource(s).toString());

		note.play();
	}

	
public void pasarPagina(ActionEvent e) throws IOException{
	
	if (this.ver<3) {
	FXMLLoader loader = new FXMLLoader(getClass().getResource("TeoriaAFIScene.fxml"));
	root=loader.load();
	
	TeoriaAFISceneController SceneController1=loader.getController();
	SceneController1.setPagina(ver+1,this.arreglo);
	
    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
	stage = (Stage)((Node)e.getSource()).getScene().getWindow();
	scene=new Scene(root);
	stage.setScene(scene);
	stage.show();
	}
	else {
		Alert a = new Alert(Alert.AlertType.INFORMATION);
		a.setContentText("Parte teorica completada, ahora se le mostraran ejercicios practicos");
		a.showAndWait();
		
		Queue<Integer> cola=new LinkedList<Integer>();
		cola.add(0);
		cola.add(1);
		cola.add(2);
		cola.add(3);
		
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("LeccionAFIScene.fxml"));
		root=loader.load();
		
		LeccionAFISceneController SceneController1=loader.getController();
		SceneController1.setPagina(cola,0);
		
	    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
		stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
		
	}
		
	}
public void cerrar(ActionEvent e) throws IOException{
	FXMLLoader loader = new FXMLLoader(getClass().getResource("AFIScene.fxml"));
	root=loader.load();

	
    //root= FXMLLoader.load(getClass().getResource("TeoriaScene.fxml"));
	stage = (Stage)((Node)e.getSource()).getScene().getWindow();
	scene=new Scene(root);
	stage.setScene(scene);
	stage.show();
	
}






}

