package com.unidioma.ds_project.controllers;

import java.io.IOException;

import com.unidioma.ds_project.App;

import javafx.fxml.FXML;

public class PrincipalController {
	@FXML
    private void switchToPhonetic() throws IOException {
        App.setRoot("TranscripcionScene");
    }
	
	@FXML
    private void switchToNotes() throws IOException {
        App.setRoot("TranscripcionScene");
    }
	
	@FXML
    private void switchToLessons() throws IOException {
        App.setRoot("TeoriasAFScene");
    }
}
